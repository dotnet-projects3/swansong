﻿using Microsoft.AspNetCore.Http;
using System.IO;

namespace SwanSong.Helper
{
    public class FileHelper
    {
        public static string SaveFile(IFormFile file, string folder)
        {
            var filename = System.Guid.NewGuid().ToString() + ".jpg"; 
            string path = folder + @"\" + filename;

            using (FileStream fs = System.IO.File.Create(path))
            {
                file.CopyTo(fs);
                fs.Flush();
            }

            return filename;
        }
    }
}

﻿using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Tokens;
using SwanSong.Domain;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace SwanSong.Helpers.Authentication
{
    public class AuthenticationHelper
    {      
        public static string IpAddress(HttpRequest request, HttpContext httpContext)
        {
            if (request.Headers.ContainsKey("X-Forwarded-For"))
                return request.Headers["X-Forwarded-For"];
            else
                return httpContext.Connection.RemoteIpAddress.MapToIPv4().ToString();
        }

        public static string CreateRandomToken()
        {
            using var rngCryptoServiceProvider = new RNGCryptoServiceProvider();
            var randomBytes = new byte[40];
            rngCryptoServiceProvider.GetBytes(randomBytes); 
            return BitConverter.ToString(randomBytes).Replace("-", "");
        }

        public static RefreshToken GenerateRefreshToken(string ipAddress, DateTime expires) //int expiryDays, 
        {
            return new RefreshToken
            {
                Token = AuthenticationHelper.CreateRandomToken(),
                Expires = expires,
                Created = DateTime.Now,
                CreatedByIp = ipAddress
            };
        }

        public static string GenerateJwtToken(Account account, int expiryHours, string secret)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] {
                    new Claim("id", account.Id.ToString()),
                    new Claim(JwtRegisteredClaimNames.Email, account.Email),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                }),
                Expires = DateTime.Now.AddHours(expiryHours), 
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}

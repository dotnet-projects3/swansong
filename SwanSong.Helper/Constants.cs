﻿namespace SwanSong.Helper
{
    public static class Constants
    {
        public const string DefaultImage = "music-note.jpg"; 
        public const string VerifyUrl = "{0}/register/verify-email/{1}";
        public const string ResetUrl = "{0}/reset-password/{1}";
        public const string AlreadyReadyRegisteredUrl = "{0}/forgot-password";
    }
}

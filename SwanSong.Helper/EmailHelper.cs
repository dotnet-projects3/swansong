﻿using MailKit.Net.Smtp;
using MimeKit;
using MimeKit.Text;
using SwanSong.Domain.Model.Settings;

namespace SwanSong.Helper
{
    public class EmailHelper
    { 
        public static void SendEmail(EmailSettings emailSettings, string to, string subject, string html, string from = null)
        {
            Send(emailSettings, CreateEmail(emailSettings, to, subject, html, from));
        }

        public static MimeMessage CreateEmail(EmailSettings emailSettings, string to, string subject, string html, string from = null)
        {
            var email = new MimeMessage();
            email.From.Add(MailboxAddress.Parse(from ?? emailSettings.EmailFrom));
            email.To.Add(MailboxAddress.Parse(to));
            email.Subject = subject;
            email.Body = new TextPart(TextFormat.Html) { Text = html };
            return email;
        }

        private static void Send(EmailSettings emailSettings, MimeMessage email)
        {
            using var smtp = new SmtpClient();
            smtp.Connect(emailSettings.SmtpHost, emailSettings.SmtpPort);
            smtp.Authenticate(emailSettings.SmtpUser, emailSettings.SmtpPass);
            smtp.Send(email);
            smtp.Disconnect(true);
        }
    }
}

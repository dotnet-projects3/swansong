﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SwanSong.Data.Migrations
{
    public partial class initialdb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Accounts",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PasswordHash = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AcceptTerms = table.Column<bool>(type: "bit", nullable: false),
                    Role = table.Column<int>(type: "int", nullable: false),
                    VerificationToken = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Verified = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ResetToken = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ResetTokenExpires = table.Column<DateTime>(type: "datetime2", nullable: true),
                    PasswordReset = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Created = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Updated = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Countries",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", nullable: false),
                    AddedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifiedDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Countries", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "IdentityRole",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NormalizedName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IdentityRole", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RecordLabels",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(100)", nullable: false),
                    AddedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifiedDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RecordLabels", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Studios",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(250)", nullable: false),
                    AddedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifiedDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Studios", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RefreshTokens",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AccountId = table.Column<int>(type: "int", nullable: false),
                    Token = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Expires = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Created = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedByIp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Revoked = table.Column<DateTime>(type: "datetime2", nullable: true),
                    RevokedByIp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ReplacedByToken = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RefreshTokens", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RefreshTokens_Accounts_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Artists",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(100)", nullable: false),
                    Photo = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    FormationYear = table.Column<int>(type: "int", nullable: true),
                    DisbandYear = table.Column<int>(type: "int", nullable: true),
                    CountryId = table.Column<int>(type: "int", nullable: true),
                    AddedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifiedDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Artists", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Artists_Countries_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Countries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BirthPlaces",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(100)", nullable: false),
                    CountryId = table.Column<int>(type: "int", nullable: true),
                    AddedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifiedDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BirthPlaces", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BirthPlaces_Countries_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Countries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Albums",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(120)", nullable: false),
                    Photo = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    RecordedDate = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    ReleaseDate = table.Column<DateTime>(type: "datetime2(7)", nullable: true),
                    LabelId = table.Column<int>(type: "int", nullable: true),
                    StudioId = table.Column<int>(type: "int", nullable: true),
                    ArtistId = table.Column<long>(type: "bigint", nullable: false),
                    Length = table.Column<string>(type: "nvarchar(10)", nullable: true),
                    Producers = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    Arrangers = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    Engineers = table.Column<string>(type: "nvarchar(250)", nullable: true),
                    Artwork = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    AddedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifiedDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Albums", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Albums_Artists_ArtistId",
                        column: x => x.ArtistId,
                        principalTable: "Artists",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Albums_RecordLabels_LabelId",
                        column: x => x.LabelId,
                        principalTable: "RecordLabels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Albums_Studios_StudioId",
                        column: x => x.StudioId,
                        principalTable: "Studios",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Members",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ArtistId = table.Column<long>(type: "bigint", nullable: true),
                    StageName = table.Column<string>(type: "nvarchar(150)", nullable: false),
                    FirstName = table.Column<string>(type: "nvarchar(50)", nullable: false),
                    Surname = table.Column<string>(type: "nvarchar(50)", nullable: false),
                    MiddleName = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    Photo = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    DateOfBirth = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DateOfDeath = table.Column<DateTime>(type: "datetime2", nullable: true),
                    BirthPlaceId1 = table.Column<int>(type: "int", nullable: true),
                    BirthPlaceId = table.Column<long>(type: "bigint", nullable: true),
                    IsSongWriter = table.Column<bool>(type: "bit", nullable: false),
                    AddedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifiedDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Members", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Members_Artists_ArtistId",
                        column: x => x.ArtistId,
                        principalTable: "Artists",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Members_BirthPlaces_BirthPlaceId1",
                        column: x => x.BirthPlaceId1,
                        principalTable: "BirthPlaces",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Songs",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(150)", nullable: false),
                    Length = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MemberId = table.Column<long>(type: "bigint", nullable: true),
                    AddedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifiedDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Songs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Songs_Members_MemberId",
                        column: x => x.MemberId,
                        principalTable: "Members",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AlbumSongs",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AlbumId = table.Column<long>(type: "bigint", nullable: false),
                    SongId = table.Column<long>(type: "bigint", nullable: false),
                    Order = table.Column<int>(type: "int", nullable: true),
                    Side = table.Column<int>(type: "int", nullable: true),
                    AddedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifiedDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AlbumSongs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AlbumSongs_Albums_AlbumId",
                        column: x => x.AlbumId,
                        principalTable: "Albums",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AlbumSongs_Songs_SongId",
                        column: x => x.SongId,
                        principalTable: "Songs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SongWriter",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PersonId = table.Column<long>(type: "bigint", nullable: false),
                    SongId = table.Column<long>(type: "bigint", nullable: false),
                    AddedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifiedDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SongWriter", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SongWriter_Songs_SongId",
                        column: x => x.SongId,
                        principalTable: "Songs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Countries",
                columns: new[] { "Id", "AddedDate", "ModifiedDate", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(2022, 8, 6, 16, 33, 16, 85, DateTimeKind.Local).AddTicks(9973), new DateTime(2022, 8, 6, 16, 33, 16, 91, DateTimeKind.Local).AddTicks(538), "England" },
                    { 2, new DateTime(2022, 8, 6, 16, 33, 16, 91, DateTimeKind.Local).AddTicks(1619), new DateTime(2022, 8, 6, 16, 33, 16, 91, DateTimeKind.Local).AddTicks(1634), "Scotland" },
                    { 3, new DateTime(2022, 8, 6, 16, 33, 16, 91, DateTimeKind.Local).AddTicks(1638), new DateTime(2022, 8, 6, 16, 33, 16, 91, DateTimeKind.Local).AddTicks(1641), "Wales" },
                    { 4, new DateTime(2022, 8, 6, 16, 33, 16, 91, DateTimeKind.Local).AddTicks(1643), new DateTime(2022, 8, 6, 16, 33, 16, 91, DateTimeKind.Local).AddTicks(1647), "Northern Ireland" },
                    { 5, new DateTime(2022, 8, 6, 16, 33, 16, 91, DateTimeKind.Local).AddTicks(1649), new DateTime(2022, 8, 6, 16, 33, 16, 91, DateTimeKind.Local).AddTicks(1652), "United States of America" },
                    { 6, new DateTime(2022, 8, 6, 16, 33, 16, 91, DateTimeKind.Local).AddTicks(1655), new DateTime(2022, 8, 6, 16, 33, 16, 91, DateTimeKind.Local).AddTicks(1657), "Australia" },
                    { 7, new DateTime(2022, 8, 6, 16, 33, 16, 91, DateTimeKind.Local).AddTicks(1660), new DateTime(2022, 8, 6, 16, 33, 16, 91, DateTimeKind.Local).AddTicks(1662), "Canada" }
                });

            migrationBuilder.InsertData(
                table: "IdentityRole",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "2c5e174e-3b0e-446f-86af-483d56fd7210", "07d97daa-9c48-4f48-9cdc-2125ad5d44ce", "USER", "USER" },
                    { "2c5e174e-3b0e-446f-86af-483d56fd7211", "58058982-51c7-4876-a8bb-6aa7f8310e71", "ADMIN", "ADMIN" }
                });

            migrationBuilder.InsertData(
                table: "RecordLabels",
                columns: new[] { "Id", "AddedDate", "ModifiedDate", "Name" },
                values: new object[,]
                {
                    { 9, new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(6786), new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(6788), "United Artists" },
                    { 8, new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(6780), new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(6782), "Reprise" },
                    { 7, new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(6775), new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(6777), "Island Records" },
                    { 5, new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(6764), new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(6766), "Sony Music Entertainment" },
                    { 6, new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(6769), new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(6772), "Columbia Label Group" },
                    { 3, new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(6751), new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(6755), "RCA Records" },
                    { 2, new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(6732), new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(6748), "Swan Song Records" },
                    { 1, new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(5628), new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(5672), "Atlantic Records" },
                    { 4, new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(6758), new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(6761), "Universal Music Publishing Group" }
                });

            migrationBuilder.InsertData(
                table: "Songs",
                columns: new[] { "Id", "AddedDate", "Length", "MemberId", "ModifiedDate", "Title" },
                values: new object[,]
                {
                    { 40L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2316), "5:38", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2318), "Whole Lotta Love" },
                    { 30L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2263), "3:32", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2265), "Stay Away" },
                    { 31L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2268), "3:16", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2270), "On a Plain" },
                    { 32L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2273), "3:52", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2276), "Something in the Way" },
                    { 33L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2278), "6:43", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2281), "Endless, Nameless" },
                    { 34L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2284), "1:10", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2286), "Longships" },
                    { 35L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2289), "5:13", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2291), "The Raven" },
                    { 36L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2294), "2:24", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2297), "Dead Loss Angeles" },
                    { 37L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2300), "3:26", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2302), "Ice" },
                    { 38L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2305), "3:50", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2307), "Baroque Bordello" },
                    { 39L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2310), "3:32", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2313), "Nuclear Device(The Wizard of Aus)" },
                    { 41L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2321), "4:33", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2323), "What Is and What Should Never Be" },
                    { 50L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2369), "3:17", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2372), "The Crunge" },
                    { 43L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2332), "6:20", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2335), "The Lemon Song" },
                    { 44L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2338), " 4:14", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2340), "Heartbreaker" },
                    { 45L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2343), "2:39", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2346), "Living Loving Maid(She's Just a Woman)" },
                    { 46L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2348), "4:34", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2351), "Ramble On" },
                    { 47L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2354), "5:32", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2356), "The Song Remains the Same" },
                    { 48L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2359), "7:39", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2361), "The Rain Song" },
                    { 49L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2364), "4:50", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2366), "Over the Hills and Far Away" },
                    { 29L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2257), "2:36", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2260), "Lounge Act" },
                    { 51L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2374), "3:43", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2377), "Dancing Days" },
                    { 52L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2380), "4:23", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2382), "D'yer Mak'er" },
                    { 53L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2385), "7:00", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2387), "No Quarter" }
                });

            migrationBuilder.InsertData(
                table: "Songs",
                columns: new[] { "Id", "AddedDate", "Length", "MemberId", "ModifiedDate", "Title" },
                values: new object[,]
                {
                    { 54L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2390), "4:31", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2392), "The Ocean" },
                    { 42L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2326), "4:21", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2329), "Thank You" },
                    { 28L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2252), "3:43", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2255), "Drain You" },
                    { 19L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2204), "3:27", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2207), "The North Star Grassman and the Ravens" },
                    { 26L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2242), "2:57", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2244), "Polly" },
                    { 1L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(456), "2:46", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(478), "Good Times Bad Times" },
                    { 2L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2085), "6:42", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2099), "Babe I'm Gonna Leave You" },
                    { 3L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2105), "6:28", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2108), "You Shook Me" },
                    { 4L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2111), "6:28", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2114), "Dazed and Confused" },
                    { 5L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2127), "4:34", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2130), "Your Time Is Gonna Come" },
                    { 6L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2133), "2:12", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2136), "Black Mountain Side" },
                    { 7L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2139), "2:30", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2141), "Communication Breakdown" },
                    { 8L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2144), "4:42", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2147), "I Can't Quit You Baby" },
                    { 9L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2150), "8:27", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2152), "How Many More Times" },
                    { 10L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2155), "4:28", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2158), "Late November" },
                    { 11L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2161), "4:07", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2163), "Blackwaterside(Traditional) " },
                    { 12L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2166), "3:09", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2169), "The Sea Captain" },
                    { 13L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2172), "3:20", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2174), "Down in the Flood(Bob Dylan)" },
                    { 14L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2177), "4:38", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2179), "John the Gun" },
                    { 15L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2182), "4:26", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2185), "Next Time Around" },
                    { 16L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2188), "3:24", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2191), "The Optimist" },
                    { 17L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2194), "2:42", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2196), "Let’s Jump the Broomstick(Charles Robins)" },
                    { 18L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2199), "2:38", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2201), "Wretched Wilbur" },
                    { 20L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2210), "3:22", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2212), "Crazy Lady Blues" },
                    { 21L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2215), "5:01", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2218), "Smells Like Teen Spirit" },
                    { 22L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2220), "4:14", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2223), "In Bloom" },
                    { 23L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2226), "3:39", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2228), "Come as You Are" },
                    { 24L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2231), "3:03", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2233), "Breed" },
                    { 25L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2236), "4:17", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2239), "Lithium" },
                    { 27L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2247), "Pissings    2:22", null, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2249), "Territorial" }
                });

            migrationBuilder.InsertData(
                table: "Studios",
                columns: new[] { "Id", "AddedDate", "ModifiedDate", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(953), new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(971), "Island Studios London, Sound Techniques London" },
                    { 2, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(1602), new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(1615), "914 Sound Studios, Blauvelt, New York" }
                });

            migrationBuilder.InsertData(
                table: "Artists",
                columns: new[] { "Id", "AddedDate", "CountryId", "DisbandYear", "FormationYear", "ModifiedDate", "Name", "Photo" },
                values: new object[,]
                {
                    { 1L, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(435), 1, 1980, 1967, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(453), "Led Zeppelin", "458eb53a-aba7-42a2-a5ed-526da552966e.jpg" },
                    { 8L, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(4236), 3, null, 1986, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(4246), "Manic Street Preachers", "8117dfa2-41a4-4be1-a33f-fd7484ae5d7a.jpg" },
                    { 11L, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(4269), 2, null, 1977, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(4278), "Simple Minds", "2a39824d-1c56-46fe-b4bc-b7e87dfb7bb3.jpg" },
                    { 7L, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(4220), 5, null, 1962, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(4225), "Bob Dylan", "567552b5-27d5-4a6e-acc5-449017fcd47e.jpg" },
                    { 15L, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(4349), 5, 1970, 1961, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(4353), "Jimi Hendrix", "f44802e4-ab03-4d2c-be3e-c094eced3c99.jpg" },
                    { 17L, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(4368), 5, null, 1987, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(4372), "Nirvana", "3f9452b1-99c8-4a79-89bd-bd5db4c2d908.jpg" },
                    { 19L, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(4384), 5, null, null, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(4387), "Tori Amos", "default-band-image.jpg" },
                    { 14L, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(4328), 6, null, 1973, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(4339), "ACDC", "85d9174f-1f0d-4221-8635-f86c211caa4d.jpg" },
                    { 4L, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(4177), 5, null, null, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(4186), "John Denver", "f94fb03e-c368-4562-9c66-2b2db5722252.jpg" },
                    { 20L, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(4391), 1, null, 1975, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(4393), "U2", "default-band-image.jpg" },
                    { 3L, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(4163), 5, null, null, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(4170), "Bruce Springsteen", "32ce9c93-9129-4bff-b166-db5ada039757.jpg" },
                    { 16L, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(4359), 1, null, 1967, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(4363), "Fleetwood Mac", "c75ac394-e941-412a-8f83-701b7aeb1e83.jpg" },
                    { 13L, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(4297), 1, null, 1975, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(4303), "Motorhead", "2691c7fa-2546-401e-a10b-1608e00711d2.jpg" },
                    { 12L, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(4288), 1, null, 1968, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(4292), "Deep Purple", "5863254c-693f-43e3-b0f8-a7ebdeb8ec33.jpg" },
                    { 10L, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(4260), 1, null, 1974, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(4264), "The Stranglers", "8b728b1e-c506-4d08-b87d-9f320f4cc920.jpg" },
                    { 9L, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(4251), 1, null, 1979, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(4254), "Ozzy Osbourne", "99cc2dbe-5671-4eca-947d-8f7a4f1759af.jpg" },
                    { 6L, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(4212), 1, null, null, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(4216), "John Lennon", "e0ceb7ed-5481-4cc1-926f-59cf31fd2e69.jpg" },
                    { 5L, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(4196), 1, null, null, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(4204), "Elton John", "64fa7eb5-dca2-4ed3-a465-fa6904bba895.jpg" },
                    { 2L, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(4112), 1, null, null, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(4132), "Sandy Denny", "0ed76f7d-e5da-4e5b-87b0-a8ecb4a165cd.jpg" },
                    { 18L, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(4378), 1, null, 1975, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(4381), "Kate Bush", "default-band-image.jpg" }
                });

            migrationBuilder.InsertData(
                table: "BirthPlaces",
                columns: new[] { "Id", "AddedDate", "CountryId", "ModifiedDate", "Name" },
                values: new object[,]
                {
                    { 11, new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(9103), 5, new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(9107), "Monterey Bay" },
                    { 10, new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(9095), 5, new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(9098), "Long Branch, New Jersey" },
                    { 5, new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(9055), 5, new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(9059), "Seattle" },
                    { 14, new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(9129), 7, new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(9132), "Vancouver" },
                    { 13, new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(9120), 1, new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(9124), "Liverpool, Lancashire" },
                    { 12, new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(9111), 1, new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(9115), "Pinner, Middlesex" },
                    { 9, new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(9087), 1, new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(9090), "Merton Park, London" },
                    { 8, new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(9079), 1, new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(9083), "Reddich" },
                    { 7, new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(9072), 1, new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(9075), "Sidcup" },
                    { 4, new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(9046), 1, new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(9051), "Heston" },
                    { 3, new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(9037), 1, new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(9041), "West Bromwich" },
                    { 2, new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(9009), 1, new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(9030), "Birmingham" },
                    { 1, new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(7654), 1, new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(7691), "Aston, Birmingham" },
                    { 6, new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(9064), 3, new DateTime(2022, 8, 6, 16, 33, 16, 92, DateTimeKind.Local).AddTicks(9068), "Pontypool" }
                });

            migrationBuilder.InsertData(
                table: "SongWriter",
                columns: new[] { "Id", "AddedDate", "ModifiedDate", "PersonId", "SongId" },
                values: new object[,]
                {
                    { 4, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(7007), new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(7009), 4L, 1L },
                    { 1, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5839), new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5854), 1L, 1L },
                    { 2, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(6981), new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(6996), 2L, 1L },
                    { 3, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(7001), new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(7003), 3L, 1L },
                    { 5, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(7012), new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(7014), 5L, 10L }
                });

            migrationBuilder.InsertData(
                table: "Albums",
                columns: new[] { "Id", "AddedDate", "Arrangers", "ArtistId", "Artwork", "Engineers", "LabelId", "Length", "ModifiedDate", "Name", "Photo", "Producers", "RecordedDate", "ReleaseDate", "StudioId" },
                values: new object[,]
                {
                    { 1L, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(3517), null, 1L, null, null, 1, null, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(3543), "Led Zeppelin 1", "11d15486-fefc-463c-8b7d-c755b08466dc.jpg", null, null, new DateTime(1969, 3, 31, 0, 0, 0, 0, DateTimeKind.Unspecified), null },
                    { 11L, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9190), null, 15L, null, null, 8, null, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9192), "Electric Ladyland", "5a90f23e-c8bc-42f6-b707-d2367e9944bb.jpg", null, null, new DateTime(1968, 10, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), null },
                    { 22L, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9254), null, 7L, null, null, null, null, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9256), "Nashville Skyline", "music-note.jpg", null, null, null, null },
                    { 23L, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9259), null, 3L, null, null, null, null, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9261), "Born In The Usa", "music-note.jpg", null, null, null, null },
                    { 19L, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9236), null, 3L, null, null, null, null, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9239), "Nebraska", "music-note.jpg", null, null, null, null },
                    { 15L, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9213), null, 3L, null, null, null, null, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9216), "Darkness on the Edge of Town", "5f63ee74-d7c8-480d-8ab9-4362ccf26492.jpg", null, null, null, null },
                    { 14L, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9208), null, 3L, null, null, null, null, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9210), "Born To Run", "a4f987e1-17d4-44a6-a929-ad29002dd31b.jpg", null, null, null, null },
                    { 13L, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9202), null, 3L, null, null, null, null, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9205), "The River", "6fc79109-d0db-4a34-8d07-f7c9866ddd5e.jpg", null, null, null, null },
                    { 12L, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9196), null, 16L, null, null, null, "39:43", new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9198), "Rumours", "549c2970-fcf9-4ac4-824b-31f69a4f2159.jpg", "Fleetwood Mac, Ken Caillat, Richard Dashut", null, new DateTime(1977, 4, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), null },
                    { 20L, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9242), null, 13L, null, null, null, null, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9244), "Iron Fist", "music-note.jpg", null, null, null, null },
                    { 21L, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9248), null, 12L, null, null, null, null, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9251), "Stormbringer", "music-note.jpg", null, null, null, null },
                    { 18L, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9231), null, 10L, null, null, null, null, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9233), "Men In Black", "music-note.jpg", null, null, null, null },
                    { 17L, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9225), null, 10L, null, null, 9, null, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9228), "The Raven", "c5d29d3d-3bf8-4a60-ad9a-411b86c026dd.jpg", null, null, new DateTime(1979, 9, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), null },
                    { 16L, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9220), null, 9L, null, null, null, null, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9222), "Bark At The Moon", "7413f9cb-77fc-4a2b-9372-51e0abd9ccea.jpg", null, null, null, null },
                    { 24L, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9264), null, 17L, null, null, null, null, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9267), "Nevermind", "a3828e09-6f78-484f-80b0-d57f6a1e972b.jpg", null, null, new DateTime(1991, 8, 31, 0, 0, 0, 0, DateTimeKind.Unspecified), null },
                    { 8L, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9172), null, 1L, null, null, 2, null, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9174), "The Song Remains The Same", "d2710986-0720-4231-8582-4383eb9ffe12.jpg", null, null, new DateTime(1976, 9, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), null },
                    { 3L, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9122), null, 1L, null, null, 1, null, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9138), "Led Zeppelin 2", "ee552af1-7a5a-457b-b336-823bc1285dac.jpg", null, null, new DateTime(1969, 10, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), null },
                    { 2L, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(6707), "Sandy Denny", 2L, "Keef", "John Wood", 7, "39:41", new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(6726), "The North Star Grassman and the Ravens", "e3b74057-75df-4cfe-ba42-9ed4f57623f5.jpg", "John Wood, Richard Thompson, Sandy Denny", new DateTime(1971, 9, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1986, 9, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1 },
                    { 4L, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9147), null, 1L, null, null, 1, null, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9150), "Led Zeppelin 3", "9cf368d3-db16-4120-af40-7dd697b934fa.jpg", null, null, new DateTime(1970, 10, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), null },
                    { 5L, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9154), null, 1L, null, null, 1, null, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9157), "Led Zeppelin IV", "3e7e88f2-5fa8-4402-ad90-39d5f14f14c7.jpg", null, null, new DateTime(1971, 11, 8, 0, 0, 0, 0, DateTimeKind.Unspecified), null },
                    { 10L, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9184), null, 1L, null, null, 2, null, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9187), "In Through The OutDoor", "6fcc74a1-bd11-439a-8a71-c52f50273617.jpg", null, null, new DateTime(1979, 8, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), null },
                    { 9L, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9178), null, 1L, null, null, 2, null, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9181), "Presence", "a0ec6d77-a1f6-4118-baed-157e40c5a50b.jpg", null, null, new DateTime(1976, 3, 31, 0, 0, 0, 0, DateTimeKind.Unspecified), null },
                    { 6L, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9161), null, 1L, null, null, 1, null, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9163), "Houses of the Holy", "45e3d1ea-6cb0-43c5-93c9-b2593b4f64d4.jpg", null, null, new DateTime(1973, 3, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), null },
                    { 7L, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9167), null, 1L, null, null, 2, null, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(9169), "Physical Graffiti", "7f616f0b-8c58-4bbd-a438-0692b803ba50.jpg", null, null, new DateTime(1975, 2, 24, 0, 0, 0, 0, DateTimeKind.Unspecified), null }
                });

            migrationBuilder.InsertData(
                table: "Members",
                columns: new[] { "Id", "AddedDate", "ArtistId", "BirthPlaceId", "BirthPlaceId1", "DateOfBirth", "DateOfDeath", "FirstName", "IsSongWriter", "MiddleName", "ModifiedDate", "Photo", "StageName", "Surname" },
                values: new object[,]
                {
                    { 7L, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(187), 4L, 11L, null, new DateTime(1943, 12, 31, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1997, 10, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), "John", true, null, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(189), null, "John Denver", "Denver" },
                    { 6L, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(169), 3L, 10L, null, new DateTime(1949, 9, 23, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Bruce", true, null, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(182), null, "Bruce Springsteen", "Springsteen" },
                    { 8L, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(193), 5L, 12L, null, new DateTime(1947, 3, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Elton", true, null, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(195), null, "Elton John", "John" },
                    { 1L, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(5075), 1L, 4L, null, new DateTime(1944, 1, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "James", true, "Patrick", new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(5087), "jimmy-page.jpg", "Jimmy Page", "Page" },
                    { 2L, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(9777), 1L, 3L, null, new DateTime(1948, 8, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Robert", true, "Anthony", new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(9797), "5ebc88e6-7cbd-4c84-bc80-2c2aa832104f.jpg", "Robert Plant", "Plant" },
                    { 3L, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(9803), 1L, 7L, null, new DateTime(1946, 1, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "John", true, "Jones", new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(9806), "b80b7338-3caa-41ce-99f9-8b39e74ce21b.jpg", "John Paul Jones", "Paul" },
                    { 4L, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(9809), 1L, 8L, null, new DateTime(1948, 5, 31, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "John", true, "Bonham", new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(9812), "7c0fa2d4-07e4-45fd-8844-85dd78e2433a.jpg", "John Bonham", "Henry" },
                    { 10L, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(205), 15L, 14L, null, new DateTime(1942, 11, 27, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1970, 9, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), "James", true, "Allen", new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(208), null, "Jimi Hendrix", "Hendrix" },
                    { 5L, new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(9816), 2L, 9L, null, new DateTime(1947, 1, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1978, 4, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), "Alexandra", true, "Elene MacLean", new DateTime(2022, 8, 6, 16, 33, 16, 93, DateTimeKind.Local).AddTicks(9819), "b4961727-17ff-4594-97f1-b80d3b28ca3c.jpg", "Sandy Denny", "Denny" },
                    { 9L, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(199), 6L, 13L, null, new DateTime(1940, 10, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1980, 12, 8, 0, 0, 0, 0, DateTimeKind.Unspecified), "John", true, null, new DateTime(2022, 8, 6, 16, 33, 16, 94, DateTimeKind.Local).AddTicks(201), null, "John Lennon", "Lennon" }
                });

            migrationBuilder.InsertData(
                table: "AlbumSongs",
                columns: new[] { "Id", "AddedDate", "AlbumId", "ModifiedDate", "Order", "Side", "SongId" },
                values: new object[,]
                {
                    { 1L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2943), 1L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(2953), 1, 1, 1L },
                    { 15L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4862), 2L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4868), 6, 2, 15L },
                    { 16L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4877), 2L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4887), 7, 2, 16L },
                    { 17L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4891), 2L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4894), 8, 2, 17L },
                    { 18L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4908), 2L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4918), 9, 2, 18L },
                    { 19L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4928), 2L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4931), 10, 2, 19L },
                    { 20L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4936), 2L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4941), 11, 2, 20L },
                    { 36L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5060), 17L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5063), 7, 2, 34L },
                    { 37L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5065), 17L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5068), 1, 1, 35L },
                    { 38L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5071), 17L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5073), 2, 1, 36L },
                    { 39L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5076), 17L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5079), 3, 1, 37L },
                    { 40L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5082), 17L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5084), 4, 1, 38L },
                    { 41L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5087), 17L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5089), 5, 1, 39L },
                    { 21L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4966), 24L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4976), 1, 1, 21L },
                    { 22L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4981), 24L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4985), 2, 1, 22L },
                    { 23L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4989), 24L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4994), 3, 1, 23L },
                    { 24L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4998), 24L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5002), 4, 1, 24L },
                    { 25L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5009), 24L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5011), 5, 1, 25L },
                    { 26L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5014), 24L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5017), 6, 1, 26L },
                    { 27L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5020), 24L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5023), 1, 2, 27L },
                    { 28L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5026), 24L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5029), 2, 2, 28L },
                    { 29L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5033), 24L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5035), 3, 2, 29L },
                    { 30L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5038), 24L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5041), 4, 2, 30L },
                    { 31L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5044), 24L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5046), 5, 2, 31L },
                    { 14L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4854), 2L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4858), 5, 1, 14L },
                    { 13L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4844), 2L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4851), 4, 1, 13L },
                    { 12L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4826), 2L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4834), 3, 1, 12L },
                    { 11L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4814), 2L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4817), 2, 1, 11L },
                    { 2L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4700), 1L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4715), 2, 1, 2L },
                    { 3L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4721), 1L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4725), 3, 1, 3L },
                    { 4L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4730), 1L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4734), 4, 1, 4L },
                    { 5L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4739), 1L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4742), 5, 2, 5L },
                    { 6L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4746), 1L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4750), 6, 2, 6L },
                    { 7L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4754), 1L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4759), 7, 2, 7L },
                    { 8L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4763), 1L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4767), 8, 2, 8L },
                    { 9L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4770), 1L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4774), 9, 2, 9L },
                    { 43L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5092), 3L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5095), 1, 1, 40L },
                    { 44L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5098), 3L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5100), 2, 1, 41L },
                    { 45L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5103), 3L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5105), 4, 1, 42L },
                    { 34L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5049), 24L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5052), 6, 2, 32L },
                    { 46L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5108), 3L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5111), 3, 1, 43L },
                    { 48L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5119), 3L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5121), 2, 2, 45L }
                });

            migrationBuilder.InsertData(
                table: "AlbumSongs",
                columns: new[] { "Id", "AddedDate", "AlbumId", "ModifiedDate", "Order", "Side", "SongId" },
                values: new object[,]
                {
                    { 49L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5125), 3L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5127), 3, 2, 46L },
                    { 50L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5130), 6L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5133), 1, 1, 47L },
                    { 51L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5136), 6L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5138), 2, 1, 48L },
                    { 52L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5141), 6L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5143), 3, 1, 49L },
                    { 53L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5146), 6L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5149), 4, 1, 50L },
                    { 54L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5151), 6L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5154), 1, 2, 51L },
                    { 55L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5157), 6L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5159), 2, 2, 52L },
                    { 56L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5162), 6L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5165), 3, 2, 53L },
                    { 57L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5168), 6L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5170), 4, 2, 54L },
                    { 10L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4792), 2L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(4804), 1, 1, 10L },
                    { 47L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5114), 3L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5116), 1, 2, 44L },
                    { 35L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5055), 24L, new DateTime(2022, 8, 6, 16, 33, 16, 95, DateTimeKind.Local).AddTicks(5057), 7, 2, 33L }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Albums_ArtistId",
                table: "Albums",
                column: "ArtistId");

            migrationBuilder.CreateIndex(
                name: "IX_Albums_LabelId",
                table: "Albums",
                column: "LabelId");

            migrationBuilder.CreateIndex(
                name: "IX_Albums_StudioId",
                table: "Albums",
                column: "StudioId");

            migrationBuilder.CreateIndex(
                name: "IX_AlbumSongs_AlbumId",
                table: "AlbumSongs",
                column: "AlbumId");

            migrationBuilder.CreateIndex(
                name: "IX_AlbumSongs_SongId",
                table: "AlbumSongs",
                column: "SongId");

            migrationBuilder.CreateIndex(
                name: "IX_Artists_CountryId",
                table: "Artists",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_BirthPlaces_CountryId",
                table: "BirthPlaces",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_Members_ArtistId",
                table: "Members",
                column: "ArtistId");

            migrationBuilder.CreateIndex(
                name: "IX_Members_BirthPlaceId1",
                table: "Members",
                column: "BirthPlaceId1");

            migrationBuilder.CreateIndex(
                name: "IX_RefreshTokens_AccountId",
                table: "RefreshTokens",
                column: "AccountId");

            migrationBuilder.CreateIndex(
                name: "IX_Songs_MemberId",
                table: "Songs",
                column: "MemberId");

            migrationBuilder.CreateIndex(
                name: "IX_SongWriter_SongId",
                table: "SongWriter",
                column: "SongId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AlbumSongs");

            migrationBuilder.DropTable(
                name: "IdentityRole");

            migrationBuilder.DropTable(
                name: "RefreshTokens");

            migrationBuilder.DropTable(
                name: "SongWriter");

            migrationBuilder.DropTable(
                name: "Albums");

            migrationBuilder.DropTable(
                name: "Accounts");

            migrationBuilder.DropTable(
                name: "Songs");

            migrationBuilder.DropTable(
                name: "RecordLabels");

            migrationBuilder.DropTable(
                name: "Studios");

            migrationBuilder.DropTable(
                name: "Members");

            migrationBuilder.DropTable(
                name: "Artists");

            migrationBuilder.DropTable(
                name: "BirthPlaces");

            migrationBuilder.DropTable(
                name: "Countries");
        }
    }
}

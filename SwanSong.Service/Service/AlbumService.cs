﻿using AutoMapper;
using FluentValidation;
using FluentValidation.Results;
using Microsoft.Extensions.Caching.Memory;
using SwanSong.Data.UnitOfWork.Interfaces;
using SwanSong.Domain;
using SwanSong.Domain.Dto;
using SwanSong.Helper.Filter;
using SwanSong.Service.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SwanSong.Service
{
    public class AlbumService : BaseService<Album, AlbumDto>, IAlbumService
    {
        public AlbumService(IMapper mapper,
                            IValidator<Album> validator,
                            IMemoryCache memoryCache,
                            IUnitOfWork unitOfWork) : base(validator, memoryCache, unitOfWork, mapper)
        { }

        public async Task<long> CountAsync()
        {
            return await _unitOfWork.Albums.CountAsync();
        }

        public async Task<List<AlbumReadOnlyDto>> GetAllAsync(PaginationFilter filter)
        {
            return _mapper.Map<List<AlbumReadOnlyDto>>(await _unitOfWork.Albums.GetAllAsync(filter.PageNumber, filter.PageSize));
        }

        public async Task<List<AlbumReadOnlyDto>> GetRandomAsync(int numberOfAlbums)
        {
            return _mapper.Map<List<AlbumReadOnlyDto>>(await _unitOfWork.Albums.GetRandomAsync(numberOfAlbums)).OrderBy(a => a.Name).ToList();
        }

        public async Task<List<AlbumReadOnlyDto>> SearchByNameAsync(string criteria)
        {
            return _mapper.Map<List<AlbumReadOnlyDto>>(await _unitOfWork.Albums.SearchByNameAsync(criteria)); 
        }

        public async Task<List<AlbumReadOnlyDto>> SearchByLetterAsync(string letter)
        {
            return _mapper.Map<List<AlbumReadOnlyDto>>(await _unitOfWork.Albums.SearchByLetterAsync(letter)); 
        }

        public async Task<List<AlbumReadOnlyDto>> GetAlbumsForArtistAsync(long artistId)
        {
            return _mapper.Map<List<AlbumReadOnlyDto>>(await _unitOfWork.Albums.GetAlbumsForArtistAsync(artistId)); 
        }

        public async Task<AlbumDto> GetAsync(long id)
        {
            return _mapper.Map<AlbumDto>(await _unitOfWork.Albums.GetAsync(id));
        }
 
        public async Task<AlbumDto> SaveAsync(AlbumDto albumDto)
        {
            Album album = await GetAlbum(albumDto);
         
            ValidationResult result = BeforeSave(album);
            if (!result.IsValid)
                return GetDto(album, result.Errors, false);

            album = await SaveAsync(album); 

            return GetDto(album, AfterSave(album, null), true);
        }
         
        public async Task<AlbumDto> DeleteAsync(long id)
        {
            Album album = await _unitOfWork.Albums.GetAsync(id); 

            ValidationResult result = BeforeDelete(album);
            if (!result.IsValid)
                return GetDto(album, result.Errors, false); 

            await DeleteAsync(album); 

            return GetDto(album, AfterDelete(album, null), true);
        } 

        public async Task<AlbumDto> UpdateAlbumPhotoAsync(long id, string filename)
        {
            return _mapper.Map<AlbumDto>(await _unitOfWork.Albums.UpdateAlbumPhotoAsync(id, filename));
        }

        private async Task<Album> GetAlbum(AlbumDto albumDto)
        {
            Album currentAlbum = albumDto.Id == 0 ? new() : await _unitOfWork.Albums.GetAsync(albumDto.Id);
            return _mapper.Map<AlbumDto, Album>(albumDto, currentAlbum);
        } 

        private async Task<Album> SaveAsync(Album album)
        {
            if (album.Id == 0)
                _unitOfWork.Albums.Add(album);
            else
                _unitOfWork.Albums.Update(album);

            await _unitOfWork.Complete();

            return album;
        }

        private async Task DeleteAsync(Album album)
        {
            _unitOfWork.Albums.Remove(album);
            await _unitOfWork.Complete(); 
        }
    }
}

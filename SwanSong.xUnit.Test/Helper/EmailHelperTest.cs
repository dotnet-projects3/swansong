﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MimeKit;
using SwanSong.Domain.Model.Settings;
using SwanSong.Helper;
using Xunit;

namespace SwanSong.xUnit.Test.Helper
{
    public class EmailHelperTest
    {
        [Fact]
        public void Create_email_return_mime_message_from_passed()
        {
            EmailSettings emailSettings = new EmailSettings();
            string from = "test-from@hotmail.com";
            string to = "test@hotmail.com";
            string subject = "Test Email";
            string html = $@"<h4>Test Email</h4>";            

            MimeMessage actualMimeMessage = EmailHelper.CreateEmail(emailSettings, to, subject, html, from);

            Assert.Equal(from, actualMimeMessage.From.First().ToString());
            Assert.Equal(to, actualMimeMessage.To.First().ToString());
            Assert.Equal(subject, actualMimeMessage.Subject);
            Assert.Equal(html, actualMimeMessage.HtmlBody);
        }

        [Fact]
        public void Create_email_return_mime_message_from_in_email_settings()
        {
            EmailSettings emailSettings = new EmailSettings();
            emailSettings.EmailFrom = "test-from2@hotmail.com";
            string from = null;
            string to = "test@hotmail.com";
            string subject = "Test Email";
            string html = $@"<h4>Test Email</h4>";

            MimeMessage actualMimeMessage = EmailHelper.CreateEmail(emailSettings, to, subject, html, from);

            Assert.Equal(emailSettings.EmailFrom, actualMimeMessage.From.First().ToString());
            Assert.Equal(to, actualMimeMessage.To.First().ToString());
            Assert.Equal(subject, actualMimeMessage.Subject);
            Assert.Equal(html, actualMimeMessage.HtmlBody);
        }
    }
}

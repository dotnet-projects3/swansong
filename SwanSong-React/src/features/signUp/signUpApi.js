import { createAsyncThunk } from "@reduxjs/toolkit";
import http from "../../common/apis/HttpCommon";

export const signUp = createAsyncThunk(
  "signUp",
  async (signUpRequest, { rejectWithValue }) => {  
    try {
      const response = await http.post(
        `register`, signUpRequest
      );
      return response.data;
    } catch(err){
      throw rejectWithValue(err.response.data.message)
    }    
  }
);

export const verifySignUp = createAsyncThunk(
  "verifySignUp",
  async (verifySignUpRequest, { rejectWithValue }) => {  
    try {
      const response = await http.post(
        `register/verify-email`, verifySignUpRequest
      );
      return response.data;
    } catch(err){
      throw rejectWithValue(err.response.data.message)
    }    
  }
);
import { createSlice } from "@reduxjs/toolkit";
import { signUp, verifySignUp } from "./signUpApi" 

const initialState = { 
  signUpRequestFailed: false,
  signUpRequestCompleted: false,
  verifySignUpRequestFailed: false,
  verifySignUpRequestCompleted: false,
  httpError: null
};

const signUpSlice = createSlice({
  name: "signUp",
  initialState, 
  extraReducers: {
    [signUp.pending]: (state) => {
      console.log("Pending");
      return { ...state, signUpRequestFailed: false, signUpRequestCompleted: false, httpError: "" }
    },
    [signUp.fulfilled]: (state, { payload }) => {
      console.log("Sign Up Request Successfully!");
      return { ...state, signUpRequestCompleted: true };
    },
    [signUp.rejected]: (state, action) => {
      console.log("Sign Up Request Rejected!"); 
      return { ...state, signUpRequestFailed: true, httpError: action.payload }
    },
    [verifySignUp.pending]: (state) => {
      console.log("Pending");
      return { ...state, verifySignUpRequestFailed: false, verifySignUpRequestCompleted: false, httpError: "" }
    },
    [verifySignUp.fulfilled]: (state, { payload }) => {
      console.log("Sign Up Request Successfully!");
      return { ...state, verifySignUpRequestCompleted: true };
    },
    [verifySignUp.rejected]: (state, action) => {
      console.log("Sign Up Request Rejected!"); 
      return { ...state, verifySignUpRequestFailed: true, httpError: action.payload }
    },
  },
});
 
export const SelectSignUpRequestFailed = (state) => state.signUp.signUpRequestFailed
export const SelectSignUpRequestCompleted = (state) => state.signUp.signUpRequestCompleted
export const SelectVerifySignUpRequestFailed = (state) => state.signUp.verifySignUpRequestFailed
export const SelectVerifySignUpRequestCompleted = (state) => state.signUp.verifySignUpRequestCompleted
export const SelectHttpError = (state) => state.signUp.httpError
export default signUpSlice.reducer;

import { configureStore } from "@reduxjs/toolkit";
import moviesReducer from "./movies/movieSlice";
import authenticationReducer from "./authentication/authenticationSlice";
import signUpReducer from "./signUp/signUpSlice"
import passwordResetReducer from "./resetPassword/resetPasswordSlice"
import albumsReducer from './albums/albumSlice'

export const store = configureStore({
  reducer: {
    movies: moviesReducer,
    authentication: authenticationReducer,
    signUp: signUpReducer,
    resetPassword: passwordResetReducer,
    albums: albumsReducer
  },
});

import { createSlice } from "@reduxjs/toolkit";
import { requestResetPassword, verifyResetPasswordToken, resetPassword } from "./resetPasswordApi" 

const initialState = { 
  resetPasswordRequestFailed: false,
  resetPasswordRequestCompleted: false,
  verifyResetPasswordRequestFailed: false,
  verifyResetPasswordRequestCompleted: false,
  resetPasswordFailed: false,
  resetPasswordCompleted: false,
  httpError: null
};

const resetPasswordSlice = createSlice({
  name: "resetPassword",
  initialState, 
  extraReducers: {
    [requestResetPassword.pending]: (state) => {
      console.log("Request Reset Password Pending");
      return { ...state, resetPasswordRequestFailed: false, resetPasswordRequestCompleted: false, httpError: "" }
    },
    [requestResetPassword.fulfilled]: (state, { payload }) => {
      console.log("Request Reset Password Successfully!");
      return { ...state, resetPasswordRequestCompleted: true };
    },
    [requestResetPassword.rejected]: (state, action) => {
      console.log("Request Reset Password Rejected!"); 
      return { ...state, resetPasswordRequestFailed: true, httpError: action.payload }
    },
    [verifyResetPasswordToken.pending]: (state) => {
      console.log("Verify Reset Password Pending");
      return { ...state, verifyResetPasswordRequestFailed: false, verifyResetPasswordRequestCompleted: false, httpError: "" }
    },
    [verifyResetPasswordToken.fulfilled]: (state, { payload }) => {
      console.log("Verify Reset Password Successfully!");
      return { ...state, verifyResetPasswordRequestCompleted: true };
    },
    [verifyResetPasswordToken.rejected]: (state, action) => {
      console.log("Verify Reset Password  Rejected!"); 
      return { ...state, verifyResetPasswordRequestFailed: true, httpError: action.payload }
    },  
    [resetPassword.pending]: (state) => {
      console.log("Reset Password Pending");
      return { ...state, resetPasswordFailed: false,resetPasswordCompleted: false, httpError: "" }
    },
    [resetPassword.fulfilled]: (state, { payload }) => {
      console.log("Reset Password Successfully!");
      return { ...state, resetPasswordCompleted: true };
    },
    [resetPassword.rejected]: (state, action) => {
      console.log("Reset Password Rejected!"); 
      return { ...state, resetPasswordFailed: true, httpError: action.payload }
    },
  },
});
 
export const SelectResetPasswordRequestFailed = (state) => state.resetPassword.resetPasswordRequestFailed
export const SelectResetPasswordRequestCompleted = (state) => state.resetPassword.resetPasswordRequestCompleted
export const SelectVerifyResetPasswordRequestFailed = (state) => state.resetPassword.verifyResetPasswordRequestFailed
export const SelectVerifyResetPasswordRequestCompleted = (state) => state.resetPassword.verifyResetPasswordRequestCompleted
export const SelectResetPasswordFailed = (state) => state.resetPassword.resetPasswordFailed
export const SelectResetPasswordCompleted = (state) => state.resetPassword.resetPasswordCompleted
export const SelectHttpError = (state) => state.resetPassword.httpError
export default resetPasswordSlice.reducer;

import { createAsyncThunk } from "@reduxjs/toolkit";
import http from "../../common/apis/HttpCommon";

export const requestResetPassword = createAsyncThunk(
  "requestResetPassword",
  async (resetPasswordRequest, { rejectWithValue }) => {  
    try {
      const response = await http.post(
        `forgot-password`, resetPasswordRequest
      );
      return response.data;
    } catch(err){
      throw rejectWithValue(err.response.data.message)
    }    
  }
);

export const verifyResetPasswordToken = createAsyncThunk(
  "verifyResetPasswordToken",
  async (verifyResetPasswordTokenRequest, { rejectWithValue }) => {  
    try {
      const response = await http.post(
        `forgot-password/validate-reset-token`, verifyResetPasswordTokenRequest
      );
      return response.data;
    } catch(err){
      throw rejectWithValue(err.response.data.message)
    }    
  }
);

export const resetPassword = createAsyncThunk(
  "resetPassword",
  async (resetPassword, { rejectWithValue }) => {  
    try {
      const response = await http.post(
        `forgot-password/reset-password`, resetPassword
      );
      return response.data;
    } catch(err){
      throw rejectWithValue(err.response.data.message)
    }    
  }
);
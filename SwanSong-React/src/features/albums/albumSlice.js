import { createSlice } from "@reduxjs/toolkit";
import { albumSearchByWord } from "./albumApi" 

const initialState = {
  albums: [] 
};

const albumsSlice = createSlice({
  name: "albums",
  initialState, 
  // reducers: {
  //   logout: (state) => {
  //     state.user = null
  //     state.jwtToken = null
  //     state.refreshToken = null
  //     state.isAuthenticated = null 
  //   },  
  //   updateToken: (token, refreshToken) => (state) => {
  //  // updateTokens: (state, token, refreshToken) => {
  //     let a = token
  //     let b = refreshToken
  //   }
  // },
  extraReducers: {
    [albumSearchByWord.pending]: (state) => {
      console.log("Pending");
      return { ...state, albums: []  }
    },
    [albumSearchByWord.fulfilled]: (state, { payload }) => {
      console.log("Fetched Successfully!");
      return { ...state, albums: payload };
    },
    [albumSearchByWord.rejected]: (state) => {
      console.log("Rejected!");
      return { ...state, albums: []  }
    },
  },
});
 
export const selectAlbums = (state) => state.albums  
export default albumsSlice.reducer



// import { createSlice } from '@reduxjs/toolkit'
// import { albumApi, AlbumSearchResponse } from './albumApi'
// import type { RootState } from 'store' 

// type AlbumState = {   
//   albumSearchResults: AlbumSearchResponse[] | null
// }
 
// const slice = createSlice({
//   name: 'album',
//   initialState: { albumSearchResults: [] } as AlbumState,
//   reducers: {
//     // logout: (state) => {
//     //   state.user = null
//     //   state.jwtToken = null
//     //   state.isAuthenticated = false
//     // },
//   },
//   extraReducers: (builder) => {     
//     builder.addMatcher(
//     albumApi.endpoints.getAlbums.matchFulfilled,
//       (state, action) => { 
//         state.albumSearchResults = action.payload 
//        // [
//         // ...state,
//         //         {
//         //             title: action.payload.title,
//         //             artist: action.payload.artist,
//         //             audioSrc: action.payload.audioSrc,
//         //             img: action.payload.img
//         //         }
//         //       ]
//       },
//     )
//   },
// })

// //export const { logout } = slice.actions

// export default slice.reducer
// export const albumSearchResults = (state: RootState) => state.albums.albumSearchResults
import React, {useEffect,} from "react";
import { BrowserRouter as Router, Route, Switch, useHistory } from "react-router-dom";
import { useSelector } from 'react-redux'
import Home from "./components/Home/Home";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import { PrivateRoute } from './components/Authentication/PrivateRoute' 
import { selectCurrentUser, selectIsAuthenticated } from './features/authentication/authenticationSlice'
//import { ROLE } from './features/authentication/authenticationApi'
import PageNotFound from "./components/PageNotFound/PageNotFound";
import MovieDetail from "./components/MovieDetail/MovieDetail";
import Login from './components/Authentication/Login'
import SignUp from './components/SignUp/SignUp'
import VerifySignUp from './components/SignUp/VerifySignUp'
import RequestResetPassword from "./components/ResetPassword/RequestResetPassword";
import ResetPassword from "./components/ResetPassword/ResestPassword"
import Admin from './components/Admin/Admin'
import Artists from './components/Admin/Artists/Artists'
import "./App.scss";

function App() {



  // export enum ROLE {
  //   ADMIN = 'Admin',
  //   USER = 'User',
  // }
  
  

  function Header() { 
    const user = useSelector(selectCurrentUser)
    const isAuthenticated = useSelector(selectIsAuthenticated)    
    //const navigate = useNavigate()
    const history = useHistory();

    useEffect(() => { 
      if (user && 'Admin' === user.role) { 
        history.push ('/admin/home')
      } else if (user && 'User' === user.role) { 
        history.push ('/user/home')
      } else {
        history.push ('/login')
      }   
    }, [isAuthenticated]); // triggers when isUserLoggedIn changes
  
    return (
      <div></div> 
    )
  }

  return (
    <div className="app">
      <Router>
        <Header></Header>
        <div>
          <Switch>
            {/* <Route path="/" component={Login} /> */}
            <Route path="/login" component={Login} />
            <Route path="/sign-up/verify-email/:token" component={VerifySignUp} />    
            <Route path="/sign-up" component={SignUp} />
            <Route path="/reset-password-request" component={RequestResetPassword} />
            <Route path="/reset-password/:token" component={ResetPassword} />  
            {/* <Route path="/admin/home" element={<PrivateRoute component={AdminHome} roles={['Admin']}  />} />   */}

            <PrivateRoute path="/admin/home">
              <Admin></Admin>
            </PrivateRoute>

            <PrivateRoute path="/admin/artists">
              <Admin></Admin>    
            </PrivateRoute>

            <PrivateRoute path="/admin/albums">
              <Admin></Admin>    
            </PrivateRoute>

            <PrivateRoute path="/admin/songs">
              <Admin></Admin>    
            </PrivateRoute>

            <PrivateRoute path="/admin/members">
              <Admin></Admin>    
            </PrivateRoute>

            <PrivateRoute path="/admin/lookups">
              <Admin></Admin>    
            </PrivateRoute>

            {/* <Route path="/admin/home" component={AdminHome} />   */}

            {/* <Route path="/" exact component={Home} />
            <Route path="/movie/:imdbID" component={MovieDetail} />             
            <Route component={PageNotFound} /> */}
          </Switch>
        </div>
        {/* <Footer /> */}
      </Router>
    </div>
  );
}

export default App;

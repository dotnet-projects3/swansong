import axiosInstance from "./HttpCommon"; 

const axiosInterceptors = (store) => {
  axiosInstance.interceptors.request.use(
    (config) => {
      const token = getLocalAccessToken();
      if (token) {
        config.headers["Authorization"] = 'Bearer ' + token;
      }
      return config;
    },
    (error) => {
      return Promise.reject(error);
    }
  );
 
  axiosInstance.interceptors.response.use(
    (res) => {
      return res;
    },
    async (err) => {
      const originalConfig = err.config;

      const publicPages = ['login', 'sign-up', 'sign-up/verify-email', '/reset-password-request', '/reset-password'];
  		let authRequired = !publicPages.includes(err.response.config.url); 

      if (authRequired && err.response) { 
        // Access Token was expired        
        if (err.response.status === 401 && !originalConfig._retry) {
          originalConfig._retry = true;

          try {            
            await axiosInstance.post("/refresh-token", getLocalRefreshToken()).then((response) => {
              console.log(response);
              localStorage.setItem('jwtToken', JSON.stringify(response.data.jwtToken));
              localStorage.setItem('refreshToken', JSON.stringify(response.data.refreshToken)); 
            }, (error) => {
              console.log(error);
            }); 
            return axiosInstance(originalConfig);
          } catch (_error) {
            return Promise.reject(_error);
          }
        }
      }

      return Promise.reject(err);
    }
  );
};

export default axiosInterceptors;






 
function getLocalAccessToken() {
	let jwtToken = window.localStorage.getItem("jwtToken");
	jwtToken = JSON.parse(jwtToken);
	if(jwtToken) {
		return jwtToken;
	}
	return "";
}
 
function getLocalRefreshToken() {
	let refreshToken = window.localStorage.getItem("refreshToken");
	refreshToken = JSON.parse(refreshToken);
	if (refreshToken) {
		return { 
			refreshToken: refreshToken
		};
	}
	return ""; 
} 
import { Alert } from '@mui/material';
 
const AlertMessage = (props: any) => {   
	return (     
		<Alert variant="filled" severity="error" style={{width:'100%'}} sx={{
			marginTop: 2,
			marginBottom: 2 
		}} >{ props.message }</Alert>
	) 
}

export default AlertMessage
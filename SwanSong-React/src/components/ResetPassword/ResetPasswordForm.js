
import React, { useState } from "react";       
import { Button, Grid, TextField} from '@mui/material';
import { resetPassword } from '../../features/resetPassword/resetPasswordApi';  
import { useDispatch, useSelector } from "react-redux"; 
import AlertMessage from '../AlertMessage'; 
import { SelectHttpError, SelectResetPasswordFailed } from '../../features/resetPassword/resetPasswordSlice'

const ResetPasswordForm = (props) => {

	const dispatch = useDispatch();   
	const resetPasswordFailed = useSelector(SelectResetPasswordFailed)
  const httpError = useSelector(SelectHttpError)
	
	const [resetPasswordRequest, setResetPasswordRequest] = React.useState({ 
		token: props.token,        
		password: 'Password#1',
		confirmPassword: 'Password#1',
	})  
 
	const handleOnResetPassword = async (e) => {
		e.preventDefault();   
		await dispatch(resetPassword(resetPasswordRequest)) 
	} 

	const handleOnChange  = (e) => {
		const { name, value } = e.target
		setResetPasswordRequest({ ...resetPasswordRequest, [name]: value }) 
	}

	let alert;  
	if(resetPasswordFailed)
		alert = <AlertMessage message = { httpError } /> 
	 
	return (
		<form onSubmit={handleOnResetPassword} >
			{alert}
			<Grid container spacing={2}>              
				<Grid item xs={12}>
					<TextField
						required
						fullWidth
						name="password"
						label="Password"
						type="password"
						id="password"
						value={resetPasswordRequest.password}
						autoComplete="new-password"
						onChange={(e) => {handleOnChange(e)}}
				/>
				</Grid>
					<Grid item xs={12}>
						<TextField
							required
							fullWidth
							name="confirmPassword"
							label="Confirm Password"
							type="password"
							id="confirmPassword"
							value={resetPasswordRequest.confirmPassword}
							autoComplete="confirm-password"
							onChange={(e) => {handleOnChange(e)}}
					/>
				</Grid>              
			</Grid>
			<Button
				type="submit"
				fullWidth
				variant="contained"
				sx={{ mt: 3, mb: 2 }}
				className="password-reset-button"
			>
				Reset Password
			</Button>  
		</form>        
	) 
}

export default ResetPasswordForm
import { useEffect } from "react";    
import { useHistory, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from "react-redux"; 
import { Button,Box, Container, CssBaseline} from '@mui/material'; 
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { verifyResetPasswordToken } from '../../features/resetPassword/resetPasswordApi';   
import { SelectResetPasswordCompleted, SelectVerifyResetPasswordRequestFailed } from '../../features/resetPassword/resetPasswordSlice'
import ResetPasswordForm from "./ResetPasswordForm.js";   
import ResetPasswordTitle from "./ResetPasswordTitle";
import AlertMessage from '../AlertMessage'
import Copyright from '../Copyright'; 

const theme = createTheme();

const ResetPassword = () => {
   
	const dispatch = useDispatch();
	const { token } = useParams()
	const history = useHistory()  
	
	useEffect(() => {         
		async function verifyPasswordResetToken() { 
			await dispatch(verifyResetPasswordToken({ token: token }))
		}    
		verifyPasswordResetToken()   
	}, [])  
   
	const verifyResetPasswordRequestFailed = useSelector(SelectVerifyResetPasswordRequestFailed)
	const resetPasswordCompleted = useSelector(SelectResetPasswordCompleted)

	let html;
	let alert;

	if(verifyResetPasswordRequestFailed)
		alert = <AlertMessage message = { "Invalid Token" } /> 

	if(resetPasswordCompleted)
		html = <>
		<p className="password-reset-verifying">Your password has been reset, you can now login.</p>
		<Button className="login-button" onClick={() => history.push("/login")}> Login</Button>    
	</> 
	else          
		html = 
			<Box sx={{ mt: 3 }}>
				<ResetPasswordForm token = { token } /> 
			</Box>
		
	return (
		<ThemeProvider theme={theme}>
			<Container component="main" maxWidth="xs">
				<CssBaseline />
				<Box
					sx={{
						marginTop: 8,
						display: 'flex',
						flexDirection: 'column',
						alignItems: 'center',
					}}
				>
					<ResetPasswordTitle title = { "Reset Password" } />					 
					{alert}
					{html} 
				</Box>
				<Copyright sx={{ mt: 5 }} />
			</Container>
		</ThemeProvider> 
	) 
}

export default ResetPassword 
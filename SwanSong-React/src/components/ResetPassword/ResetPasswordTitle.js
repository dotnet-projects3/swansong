import { Avatar, Typography } from '@mui/material';
import PasswordOutlinedIcon from '@mui/icons-material/PasswordOutlined'; 
import "./ResetPassword.scss";

const ResetPasswordTitle = (props) => {    
	return (     
		<>
		<Avatar className='reset-password-logo'>
			<PasswordOutlinedIcon />
		</Avatar>
		<Typography component="h1" variant="h5">
			{props.title}
		</Typography>
		</>
	) 
}
    
export default ResetPasswordTitle
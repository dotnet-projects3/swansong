
import { Box, Container, CssBaseline} from '@mui/material'; 
import { createTheme, ThemeProvider } from '@mui/material/styles'; 
import Copyright from '../Copyright';  
import ResetPasswordTitle from "./ResetPasswordTitle";
import RequestResetPasswordForm from "./RequestResetPasswordForm";
import { SelectResetPasswordRequestCompleted } from '../../features/resetPassword/resetPasswordSlice'
import { useSelector } from "react-redux";

const theme = createTheme();

const RequestResetPassword = () => {
    
  const resetPasswordRequestCompleted = useSelector(SelectResetPasswordRequestCompleted) 
  
  let html;
  if(resetPasswordRequestCompleted === true) {
    html = <p className="password-reset">Check emails for password request link</p>
  } else  {
    html = <Box className="request-reset-box" sx={{ mt: 3 }}>
            <RequestResetPasswordForm />
          </Box> 
  } 
  
  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        > 
          <ResetPasswordTitle title = { "Request Reset Password" } />           
          {html} 
        </Box>
        <Copyright sx={{ mt: 5 }} />
      </Container>
    </ThemeProvider>     
  )
}

export default RequestResetPassword 
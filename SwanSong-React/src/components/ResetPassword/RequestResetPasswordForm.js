import { useState } from "react";   
import { TextField, Button, Grid } from '@mui/material'; 
import { requestResetPassword } from '../../features/resetPassword/resetPasswordApi'
import AlertMessage from '../AlertMessage'
import { useDispatch, useSelector } from "react-redux";

import { SelectHttpError, SelectResetPasswordRequestFailed } from '../../features/resetPassword/resetPasswordSlice'

const RequestResetPasswordForm = () => {  
    
//	const [passwordReset] = forgottenPasswordApi.usePasswordResetMutation() 
	const [email, setEmail] = useState("Test100@hotmail.com"); 
	//const [failedToFindEmail, setFailedToFindEmail] = useState(false) 

	const dispatch = useDispatch();  

	const resetPasswordRequestFailed = useSelector(SelectResetPasswordRequestFailed)
  const httpError = useSelector(SelectHttpError)

	const handleOnRequestResetPassword = async (e) => {
		e.preventDefault();
		await dispatch(requestResetPassword({ email: email })) 
	} 

	const handleOnChange  = (e) => {
		const { value } = e.target
		setEmail(value) 
	} 

	let alert;

  	if(resetPasswordRequestFailed)
    	alert = <AlertMessage message = { httpError } />
      
	return (     
		<form onSubmit={handleOnRequestResetPassword} >
			{alert}
			<Grid container spacing={2}>              
				<Grid item xs={12}>
					<TextField
						margin="normal"
						required
						fullWidth
						id="email"
						value={email}
						label="Email Address"
						name="email"
						autoComplete="email"
						autoFocus
						onChange={(e) => {handleOnChange(e)}}
					/>             
				</Grid> 
			</Grid>       
			<Button
				type="submit"
				fullWidth
				variant="contained"
				// sx={{ mt: 3, mb: 2 }}
				className="password-reset-request-button"	
			>
				Request Password Reset
			</Button> 
		</form>
	) 
}
    
export default RequestResetPasswordForm
import { useState, useEffect } from "react";   
import { useParams, useHistory } from 'react-router-dom'; 
import { useDispatch, useSelector } from "react-redux";
import { Box, Typography, Button, Stack, CssBaseline, Container } from '@mui/material';  
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { verifySignUp } from '../../features/signUp/signUpApi'  
import Copyright from '../Copyright';
import SignUpTitle from './SignUpTitle'

import { SelectVerifySignUpRequestFailed, SelectHttpError, SelectVerifySignUpRequestCompleted } from '../../features/signUp/signUpSlice'

const theme = createTheme();

const VerifySignUp = () => {

  const { token } = useParams()
	const dispatch = useDispatch();   
	const history = useHistory(); 
	const [verificationCalled, setVerificationCalled] = useState(false)     
	
	useEffect(() => {         
		async function verifysignUp() { 
			await dispatch(verifySignUp({ token: token }))   
			setVerificationCalled(true)             
		}       
		verifysignUp()  
	}, [])

	const verifySignUpRequestCompleted = useSelector(SelectVerifySignUpRequestCompleted)
	const verifySignUpRequestFailed = useSelector(SelectVerifySignUpRequestFailed)
  const httpError = useSelector(SelectHttpError)


	let html;

	if(verificationCalled === false)
		html = <p className="sign-up-verifying">Verifying signUp...</p>
	else {
		if (verifySignUpRequestCompleted === true) {
			html = <>
							<p className="sign-up-verifying">Sign up has been completed, you can now login.</p>
							<Button className="verified-sign-up-login-button" onClick={() => history.push("/login")}> Login</Button>    
						</> 
		} else if (verifySignUpRequestFailed === true) {
			html = <p className="sign-up-verifying">Sign up verification failed, this could be because it was completed previously. { httpError }</p>         
		}
	}
		
	return (
		<ThemeProvider theme={theme}>
			<Container component="main" maxWidth="xs">
				<CssBaseline />
					<Box
						sx={{
							marginTop: 8,
							display: 'flex',
							flexDirection: 'column',
							alignItems: 'center',
						}}
						>
						<SignUpTitle title={ "Verify Sign Up" } />
						<Stack direction={{ xs: 'column', sm: 'row' }} spacing={{ xs: 1, sm: 2, md: 4 }} >
								<Typography variant="body2" color="text.secondary">
										{html}  
								</Typography>
						</Stack>       
					</Box>
				<Copyright sx={{ mt: 5 }} />
			</Container>
		</ThemeProvider>
	)  

}

export default VerifySignUp
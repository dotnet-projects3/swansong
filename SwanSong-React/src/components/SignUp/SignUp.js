import { useSelector } from "react-redux";  
import { Box, Container, CssBaseline} from '@mui/material'; 
import { createTheme, ThemeProvider } from '@mui/material/styles'; 
import { SelectSignUpRequestCompleted } from '../../features/signUp/signUpSlice'
import SignUpTitle from './SignUpTitle'
import SignUpForm from "./SignUpForm";
import Copyright from '../Copyright';  

const theme = createTheme();

const SignUp = () => {

  const signUpRequestCompleted = useSelector(SelectSignUpRequestCompleted)
 
  let html; 
  if(signUpRequestCompleted === true) { 
    html = <p className="sign-up-verifying">Check emails for sign up verification</p>
  } else {
    html =  <Box sx={{ mt: 3 }}>
              <SignUpForm />         
            </Box>  
  }

  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        > 
          <SignUpTitle title={ "Sign up" } />  
          {html} 
        </Box>  
        <Copyright sx={{ mt: 5 }} />
      </Container>
    </ThemeProvider>
  );
} 

export default SignUp
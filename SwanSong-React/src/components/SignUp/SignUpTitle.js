import { Avatar, Typography } from '@mui/material';
import AppRegistrationOutlinedIcon from '@mui/icons-material/AppRegistrationOutlined';
import "./SignUp.scss";

const SignUpTitle = (props) => {
  return (     
		<>
		<Avatar className='sign-up-logo' >
			<AppRegistrationOutlinedIcon />
		</Avatar>
		<Typography component="h1" variant="h5">
			{props.title}
		</Typography>
		</>
	) 
}

export default SignUpTitle
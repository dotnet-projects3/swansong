import { useState } from "react"; 
import { Grid, TextField, Button, Link } from '@mui/material'; 
import { signUp } from '../../features/signUp/signUpApi'
import AlertMessage from "../AlertMessage";
import { useDispatch, useSelector } from "react-redux";
import { SelectSignUpRequestFailed, SelectHttpError } from '../../features/signUp/signUpSlice'
 
const SignUpForm = () => {

  const dispatch = useDispatch();  
	const [signUpRequest, setSignUpRequest] = useState({ 
		title: "Mr",
		lastName: 'Test',
		firstName: 'User',
		email: 'Test100@hotmail.com',
		password: 'Password#1',
		confirmPassword: 'Password#1',
		acceptTerms: true
	});

	const handleOnChange  = (e) => {
		const { name, value } = e.target
		setSignUpRequest({ ...signUpRequest, [name]: value }) 
	} 
	
	const handleOnSignUp = async (e) => {
		e.preventDefault(); 
    await dispatch(signUp(signUpRequest))
	};
 
  const signUpRequestFailed = useSelector(SelectSignUpRequestFailed)
  const httpError = useSelector(SelectHttpError)

	let alert
  if(signUpRequestFailed)   
    alert = <AlertMessage message = { httpError } /> 

	return (     
		<form onSubmit={handleOnSignUp}>
			{alert}
			<Grid container spacing={2}>
				<Grid item xs={12} sm={6}>
					<TextField
						autoComplete="given-name"
						name="firstName"
						required
						fullWidth                  
						id="firstName"
						value={signUpRequest.firstName}
						label="First Name"
						autoFocus
						onChange={(e) => {handleOnChange(e)}}
					/>
				</Grid>
				<Grid item xs={12} sm={6}>
					<TextField
						required
						fullWidth
						id="lastName"
						value={signUpRequest.lastName}
						label="Last Name"
						name="lastName"
						autoComplete="family-name"
						onChange={(e) => {handleOnChange(e)}}
					/>
				</Grid>
				<Grid item xs={12}>
					<TextField
						required
						fullWidth
						id="email"
						value={signUpRequest.email}
						label="Email Address"
						name="email"
						autoComplete="email"
						onChange={(e) => {handleOnChange(e)}}
					/>
				</Grid>
				<Grid item xs={12}>
					<TextField
						required
						fullWidth
						name="password"
						label="Password"
						type="password"
						id="password"
						value={signUpRequest.password}
						autoComplete="new-password"
						onChange={(e) => {handleOnChange(e)}}
					/>
				</Grid>
				<Grid item xs={12}>
					<TextField
						required
						fullWidth
						name="confirmPassword"
						label="Confirm Password"
						type="password"
						id="confirmPassword"
						value={signUpRequest.confirmPassword}
						autoComplete="confirm-password"
						onChange={(e) => {handleOnChange(e)}}
					/>
				</Grid>              
			</Grid>
			<Button
				type="submit"
				fullWidth
				variant="contained"
				className="sign-up-button"			
			>
				Sign Up
			</Button>
			<Grid container justifyContent="flex-end" className="options-grid">
				<Grid item>
					<Link href="/login" variant="body2" className="sign-up-verifying">
						Already have an account? Sign in
					</Link>
				</Grid>
			</Grid>
		</form> 
	) 
}

export default SignUpForm
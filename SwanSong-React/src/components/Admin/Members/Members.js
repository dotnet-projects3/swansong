import * as React from 'react';
import { CssBaseline, Typography } from '@mui/material';  
import "../AdminHome.scss";

const Members = (props) => {   
	return (     
	  <div>
      <div className='top-padder'></div>         
      <CssBaseline />
      <Typography component="h1" variant="h5">
      Members
      </Typography>   
    </div>
	) 
}
    
export default Members
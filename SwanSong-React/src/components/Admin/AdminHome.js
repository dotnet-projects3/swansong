import * as React from 'react';
import { CssBaseline, Typography } from '@mui/material';  
import "./AdminHome.scss";

const AdminHome = (props) => {   
	return (     
	  <div>
      <div className='top-padder'></div>         
      <CssBaseline />
      <Typography component="h1" variant="h5">
        Admin Home
      </Typography>   
    </div>
	) 
}
    
export default AdminHome
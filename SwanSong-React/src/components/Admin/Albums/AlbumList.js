import { useEffect } from 'react'; 
import { useDispatch, useSelector } from 'react-redux'
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper'; 
 
import { albumSearchByWord } from '../../../features/albums/albumApi'
import { selectAlbums } from '../../../features/albums/albumSlice'

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(odd)': {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));

// const imageStyle: CSSProperties = { 
//     width:50,
//     height:50,
//   };

const AlbumList  = () => {

  const dispatch = useDispatch();  

  const albums = useSelector(selectAlbums)
  
    useEffect(() => {
      async function fetchData() {
        await dispatch(albumSearchByWord('the')) 
      }
      fetchData();
    }, [])

    const image = (photo) => { 
      return <img alt="Album Cover" style={{width: '50px', height: '50px'}}  src= {"/albums/" +  photo} />
    } 
      
    let table = <></>
    if(albums !== undefined) {
        table = <Table sx={{ minWidth: 700 }} aria-label="customized table">
        <TableHead>
        <TableRow>
            <StyledTableCell></StyledTableCell>
            <StyledTableCell>Title</StyledTableCell>
            <StyledTableCell>Release Date</StyledTableCell> 
        </TableRow>
        </TableHead>
        <TableBody>
        {albums.albums.map((row) => (
            <StyledTableRow key={row.id.toString()}>
                <StyledTableCell>{image(row.photo)}</StyledTableCell> 
                <StyledTableCell>{row.name}</StyledTableCell>
                <StyledTableCell>{row.releaseDate}</StyledTableCell> 
            </StyledTableRow>
        ))} 
        </TableBody>
    </Table>
    }
 
    return (<div> 
                <TableContainer component={Paper}>   
                    {table} 
                </TableContainer>
            </div>);
} 

export default AlbumList
import { useState } from "react"; 
import { Grid, TextField, Button, Link } from '@mui/material'; 
import { login } from '../../features/authentication/authenticationApi'
import { selectLoginFailed } from '../../features/authentication/authenticationSlice'
import AlertMessage from '../AlertMessage'
import { useDispatch, useSelector } from "react-redux";
import "./Authentication.scss";

const LoginForm = (props) => { 

	const dispatch = useDispatch(); 
	const [loginRequest, setLoginRequest] = useState({ email: 'Test100@hotmail.com', password: 'Password#1' })

	const handleOnChange  = (e) => {
		const { name, value } = e.target
		setLoginRequest({ ...loginRequest, [name]: value }) 
	}  
 
	const handleOnLogin = async (e) => {
    e.preventDefault()     
		await dispatch(login(loginRequest)) 
	} 

	const loginFailed = useSelector(selectLoginFailed)
 
	let alert;

	if(loginFailed)   
  	alert = <AlertMessage message = { "Failed to login - check email/password" } />
	 
	return (  
		<form onSubmit={handleOnLogin} >
			{alert}
			<TextField
				margin="normal"
				required
				fullWidth
				id="email"
				value={loginRequest.email}
				label="Email"
				name="email"
				autoComplete="email"
				autoFocus
				className="textField"
				onChange={(e) => {handleOnChange(e)}}
			/>
			<TextField
				margin="normal"
				required
				fullWidth
				name="password"
				value={loginRequest.password}
				label="Password"
				type="password" 
				id="password"
				className="textField"
				autoComplete="current-password"
				onChange={(e) => {handleOnChange(e)}}
			/>           
			<Button
				type="submit"
				fullWidth
				variant="contained"
				className="login-button"				
			>
				Sign In
			</Button>
			<Grid container className="options-grid">
				<Grid item xs>
					<Link href="/reset-password-request" variant="body2" className="reset-password-link">
						Forgot password?
					</Link>
				</Grid>
				<Grid item>
					<Link href="/sign-up" variant="body2" className="sign-up-link">
						{"Don't have an account? Sign Up"}
					</Link>
				</Grid>
			</Grid>
		</form>
	)
}

export default LoginForm
import { Box, CssBaseline, Container } from '@mui/material';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Copyright from '../Copyright';
import LoginForm from "./LoginForm";
import LoginTitle from "./LoginTitle"; 

const theme = createTheme();

const Login = () => { 
  return ( 
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >       
          <LoginTitle title = { "Login in" } /> 
          <Box>    
            <LoginForm />
          </Box>
        </Box>
        <Copyright sx={{ mt: 8, mb: 4 }} />
      </Container>
    </ThemeProvider>
  );
}

export default Login
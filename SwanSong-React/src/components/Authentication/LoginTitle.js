import { Avatar, Typography } from '@mui/material';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined'; 
import "./Authentication.scss";

const LoginTitle = (props) => {    
	return (     
		<>
			<Avatar className='login-logo' >
				<LockOutlinedIcon />
			</Avatar>
			<Typography component="h1" variant="h5">
				{props.title}
			</Typography>
		</>
	) 
}
    
export default LoginTitle
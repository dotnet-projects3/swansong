namespace SwanSong.Domain.Model.Settings
{
    public class AppSettings
    {
        public string PublicFolder { get; set; }
        public string DefaultEmail { get; set; }
        public string ClientBaseUrl { get; set; }
    }
}